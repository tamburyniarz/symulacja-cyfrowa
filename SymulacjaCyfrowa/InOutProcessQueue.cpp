#include "InOutProcessQueue.h"
#include <iostream>
using namespace std;

InOutProcessQueue::InOutProcessQueue(Generator *gener) : gen(gener) {}

bool ComparePriority::operator()(Process *left, Process *right) const
{
	if (left->priority_to_device < right->priority_to_device) return true;
	if (left->priority_to_device > right->priority_to_device) return false;

	if (left->priority_to_device_random < right->priority_to_device_random) return true;
	if (left->priority_to_device_random > right->priority_to_device_random) return false;

	return false;
}

void InOutProcessQueue::AddProcess(Process *dispatched)
{
	io_list.push(dispatched);
}

void InOutProcessQueue::removeFirst()
{
	if (!io_list.empty())
		io_list.pop();
}

Process *InOutProcessQueue::returnFirst()
{
	if (!io_list.empty())
		return io_list.top();
	else return nullptr;

	UpdatePriorities();
	if (io_list.empty()) return nullptr;
	Process *temp = io_list.top();
	io_list.pop();
	if (io_list.size() == 0) return temp;
	else
	{
		if (temp->priority_to_device > io_list.top()->priority_to_device) return temp;
		else if (temp->priority_to_device == io_list.top()->priority_to_device)
		{
			if (temp->TO > io_list.top()->TO) return temp;
			else if (temp->TO < io_list.top()->TO)
			{
				Process *temp1 = io_list.top();
				io_list.pop();
				io_list.push(temp);
				return temp1;
			}
			else
			{
				double x = gen->getNext(100);
				if (x < 0.5) return temp;
				else
				{
					Process *temp1 = io_list.top();
					io_list.pop();
					io_list.push(temp);
					return temp1;
				}
			}
		}
		else
		{
			Process *temp1 = io_list.top();
			io_list.pop();
			io_list.push(temp);
			return temp1;
		}
	}
}

void InOutProcessQueue::UpdatePriorities()
{
	std::vector<Process*> temp_list;
	while (io_list.size() != 0)
	{
		Process *temp = io_list.top();
		io_list.pop();
		temp_list.push_back(temp);
	}

	for (auto &proc : temp_list)
	{
		proc->priority_to_device = 14;
		io_list.push(proc);
	}
}

void InOutProcessQueue::display()
{
	cout << "Procesy w kolejce do urzadzenia IO:" << endl;
	while (!io_list.empty()) {
		cout << "Proces " << io_list.top()->id << " -> Priorytet: " << io_list.top()->priority_to_device << endl;
		io_list.pop();
	}
	cout << endl;
}

InOutProcessQueue::~InOutProcessQueue()
{
	Process *temp = nullptr;
	while (!io_list.empty()) {
		temp = io_list.top();
		delete temp;
		io_list.pop();
	}
}