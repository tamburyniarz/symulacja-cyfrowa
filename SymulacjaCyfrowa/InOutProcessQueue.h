#pragma once

#ifndef InOutProcessQueue_h_
#define InOutProcessQueue_h_

#include "Process.h"
#include <queue>
#include "Generator.h"

// Struktura porownujaca priorytety dwoch procesow.
struct ComparePriority
{
	bool operator()(Process* left, Process* right) const;
};


// Klasa reprezentujaca kolejke procesow oczekujacych
// na urzadzenie wejscia/wyjscia. Urzadzeniom tym przydzielane
// sa procesy z listy (kolejki) zgodnie z metoda priorytetowa.
class InOutProcessQueue
{
public:
	InOutProcessQueue(Generator *gener);
	~InOutProcessQueue();
	void AddProcess(Process *dispatched);
	void removeFirst();
	void display();
	Process *returnFirst();
	void UpdatePriorities();
private:
	Generator *gen;
	//std::priority_queue <Process*> io_list;
	std::priority_queue <Process*, std::vector<Process*>, ComparePriority> io_list;
};

#endif // !InOutProcessQueue_h_
