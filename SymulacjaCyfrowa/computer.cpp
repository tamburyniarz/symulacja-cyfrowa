#include "computer.h"
#include <iostream>

Computer::Computer(Generator* generator, DataService* data_service)
{
	generator_ = generator;
	data_service_ = data_service;
	Init();
}

Computer::~Computer()
{
}


void Computer::Init()
{
	// Reset clock time
	clock_time_ = 0;

	// Generators
	// TODO

	// Processors
	for (int i = 0; i < N_PROCESSORS; ++i)
		processors_.push_back(new Processor());
	queue_to_processor_ = new RandomQueueA2(generator_); // only one queue
	
	// IO Devices
	std::string names[5] = { "1", "2", "3", "4", "5" };
	for (int i = 0; i < N_IO_DEVICES; ++i)
	{
		devices_.push_back(new IODevice(names[i]));
		queues_to_io_device_.push_back(new PriorityQueueB4(generator_));
	}
	
	// Stats
	// TODO
}

/*
*
* Getters and setters
*
*/

std::vector<Processor*> Computer::getProcessors() const
{
	return processors_;
}

std::vector<IODevice*> Computer::getDevices() const
{
	return devices_;
}

double Computer::getClockTime() const
{
	return clock_time_;
}

void Computer::setClockTime(double clock_time)
{
	clock_time_ = clock_time;
}

RandomQueueA2* Computer::getQueueToProcessor() const
{
	return queue_to_processor_;
}

std::vector<PriorityQueueB4*> Computer::getQueuesToDevices() const
{
	return queues_to_io_device_;
}
