#pragma once
#include <vector>
#include "processor.h"
#include "io_device.h"
#include "random_queue_a2.h"
#include "priority_queue_b4.h"
#include "data_service.h"

class Computer
{
public:
	Computer(Generator * generator, DataService * data_service);
	~Computer();

	static const int N_PROCESSORS = 2;
	static const int N_IO_DEVICES = 5;

	std::vector<Processor*> getProcessors() const;
	std::vector<IODevice*> getDevices() const;
	// Getters and setters
	double getClockTime() const;
	void setClockTime(double clock_time);
	RandomQueueA2* getQueueToProcessor() const;
	std::vector<PriorityQueueB4*> getQueuesToDevices() const;
private:
	std::vector<Processor*> processors_; //vector of processors_ avaliable in the system
	std::vector <IODevice*> devices_; //vector of devices_ avaliable in the system
	RandomQueueA2 * queue_to_processor_;
	std::vector<PriorityQueueB4 *> queues_to_io_device_;

	double clock_time_;

	// TODO Generators
	Generator * generator_;

	DataService * data_service_;

	void Init();
};

