#include "conditional_event_add_process_to_device.h"
#include <iostream>



ConditionalEventAddProcessToDevice::ConditionalEventAddProcessToDevice(Computer* computer, Generator* generator, DataService* data_service, TimeEventDeviceRelease* event_device_release)
{
	computer_ = computer;
	generator_ = generator;
	data_service_ = data_service;
	event_device_release_ = event_device_release;
}

ConditionalEventAddProcessToDevice::~ConditionalEventAddProcessToDevice()
{
}

bool ConditionalEventAddProcessToDevice::Execute(int device_id, bool step)
{
	double clock_time = computer_->getClockTime();
	PriorityQueueB4* queue_ptr = computer_->getQueuesToDevices()[device_id];
	queue_ptr->update();
	IODevice* device_ptr = computer_->getDevices()[device_id];

	if (queue_ptr->top() != nullptr && !device_ptr->isBusy())
	{
		Process* process_ptr = queue_ptr->top();

		if (step)
			std::cout << "\n*ConditionalEvent* Add Process To Device: \n";

		device_ptr->AssignProcess(process_ptr);
		if (step)
			std::cout << "\t>>Added process to IO Device nr " << device_id << ". \n";

		queue_ptr->pop();
		if (step)
			std::cout << "\t>>Removed process from queue to IO Device. \n";

		process_ptr->time_in_device_queue = clock_time - process_ptr->time_start_device_queue;

		data_service_->addTotalDeviceWaitTime(process_ptr->time_in_device_queue);

		event_device_release_->setEventTime(device_id, clock_time + process_ptr->TPO);
		process_ptr->TPIO = 0;
		process_ptr->device_id = -1;

		return true;
	}
	return false;
}
