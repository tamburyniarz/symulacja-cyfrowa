#pragma once
#include "computer.h"
#include "generator.h"
#include "data_service.h"
#include "time_event_device_reealse.h"
class ConditionalEventAddProcessToDevice
{
public:
	ConditionalEventAddProcessToDevice(Computer * computer, Generator * generator, DataService * data_service, TimeEventDeviceRelease * event_device_release);
	~ConditionalEventAddProcessToDevice();
	bool Execute(int device_id, bool step); // TODO step
private:
	Computer * computer_;
	Generator * generator_;
	DataService * data_service_;
	TimeEventDeviceRelease * event_device_release_;
};

