#include "conditional_event_add_process_to_processor.h"
#include <iostream>

ConditionalEventAddProcessToProcessor::ConditionalEventAddProcessToProcessor(Computer* computer, Generator* generator, DataService* data_service, TimeEventProcessorRelease* event_proc_release, TimeEventProcessTermination* event_process_termination)
{
	computer_ = computer;
	generator_ = generator;
	data_service_ = data_service;

	event_processor_release_ = event_proc_release;
	event_process_termination_ = event_process_termination;
}

ConditionalEventAddProcessToProcessor::~ConditionalEventAddProcessToProcessor()
{
}

bool ConditionalEventAddProcessToProcessor::Execute(int processor_id, bool step)
{
	double clock_time = computer_->getClockTime();
	RandomQueueA2 * queue_ptr = computer_->getQueueToProcessor();
	Process * process_ptr = queue_ptr->top();
	Processor * processor_ptr = computer_->getProcessors()[processor_id];

	if((process_ptr != nullptr) && !(processor_ptr->isBusy()))
	{
		if (step)
			std::cout << "\n*ConditionalEvent* Add Process To Processor: \n";

		processor_ptr->AssignProcess(process_ptr);
		if (step)
			std::cout << "\t>>Added process to processor nr "<< processor_id << ". \n";

		queue_ptr->pop();
		if (step)
			std::cout << "\t>>Removed process from queue to processor. \n";

		process_ptr->time_in_processor_queue += clock_time - process_ptr->time_start_processor_queue;
		processor_ptr->setProcessEntryTime(clock_time);
		
		data_service_->addTotalProcessorWaitTime(process_ptr->time_in_processor_queue); // TODO worng

		// get random TPIO
		process_ptr->TPIO = generator_->getInteger(process_ptr->TPK);

		if(process_ptr->TPIO == 0 || process_ptr->device_id == -1) // no device demand
		{
			if (step)
				std::cout << "\t>> No device wanted. \n";
			event_process_termination_->setEventTime(processor_id, clock_time + double(process_ptr->TPW));
		}
		else
		{
			process_ptr->TPW -= process_ptr->TPIO;
			// set random device_id
			process_ptr->device_id = generator_->getInteger(Computer::N_IO_DEVICES);
			event_processor_release_->setEventTime(processor_id, clock_time + process_ptr->TPIO);

			data_service_->addDeviceDemand();

			if (step)
				std::cout << "\t>> Set processor release event time at: " << event_processor_release_->getEventTime(processor_id) << " \n";
		}

		return true;
	}
	return false;
}
