#pragma once
#include "computer.h"
#include "generator.h"
#include "data_service.h"
#include "time_event_processor_release.h"
#include "time_event_process_termination.h"

class ConditionalEventAddProcessToProcessor
{
public:
	ConditionalEventAddProcessToProcessor(Computer * computer, Generator * generator, DataService * data_service,
		TimeEventProcessorRelease * event_proc_release, TimeEventProcessTermination * event_process_termination);
	~ConditionalEventAddProcessToProcessor();
	bool Execute(int processor_id, bool step);
private:
	Computer * computer_;
	Generator * generator_;
	DataService * data_service_;

	TimeEventProcessorRelease * event_processor_release_;
	TimeEventProcessTermination * event_process_termination_;
};

