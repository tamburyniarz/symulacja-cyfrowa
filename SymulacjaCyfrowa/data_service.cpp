#define _CRT_SECURE_NO_DEPRECATE 

#include "data_service.h"
#include <ostream>
#include <fstream>
#include <iostream>


DataService::DataService()
{
	simulation_time_ = 0.0;
	avarge_processing_time_ = 0.0;
	total_processing_time_ = 0.0;
	total_capacity_ = 0.0;
	total_processor_wait_time_ = 0.0;
	avarge_processing_time_ = 0.0;
	max_processor_wait_time_ = 0.0;
	total_processor_usage_[2] = {0.0};
	avarge_device_wait_time_ = 0.0;
	total_device_wait_time_ = 0.0;
	processes_created_ = 0;
	processes_terminated_ = 0;
	n_device_demand_ = 0;
	entry_phase_ = 0;

	is_entry_phase_ = true;
}

DataService::~DataService()
{
}

void DataService::SetSimulationTime(double time)
{
	simulation_time_ = time;
}

void DataService::addTotalProcessingTime(double time)
{
	if (processes_terminated_ > entry_phase_ || !is_entry_phase_)
	{
		total_processing_time_ += time;
		is_entry_phase_ = false;
	}
}

void DataService::ComputeAvargeProcessingTime()
{
	avarge_processing_time_ = total_processing_time_ / processes_terminated_;
}

void DataService::ComputeTotalCapacity()
{
	if (processes_terminated_ > entry_phase_ || !is_entry_phase_)
	{
		total_capacity_ = processes_terminated_ / simulation_time_;
		is_entry_phase_ = false;
	}
}

void DataService::ComputeAvargeProcessorWaitTime()
{
	avargee_processor_wait_time_ = total_processor_wait_time_ / n_device_demand_;
}

void DataService::ComputeAvargeDeviceWaitTime()
{
	avarge_device_wait_time_ = total_device_wait_time_ / n_device_demand_;
}

void DataService::addTotalProcessorWaitTime(double time)
{
	if (processes_terminated_ > entry_phase_ || !is_entry_phase_)
	{
		is_entry_phase_ = false;
		total_processor_wait_time_ += time;
		if (time > max_processor_wait_time_)
			max_processor_wait_time_ = time;
	}
}

void DataService::addTotalDeviceWaitTime(double time)
{
	if (processes_terminated_ > entry_phase_ || !is_entry_phase_)
	{
		total_device_wait_time_ += time;
		is_entry_phase_ = false;
	}
}

void DataService::addTotalProcessorUsage(double value, int idCPU)
{
	if (processes_terminated_ > entry_phase_ || !is_entry_phase_)
	{
		total_processor_usage_[idCPU] += value;
		is_entry_phase_ = false;
	}
}

double DataService::getSimulationTime()
{
	return simulation_time_;
}

double DataService::getTotalProcessingTime()
{
	return total_processing_time_;
}

double DataService::getTotalCapacity()
{
	return total_capacity_;
}

double DataService::getTotalCPUtime()
{
	return total_processor_wait_time_;
}

double DataService::getTotalDeviceTime()
{
	return total_device_wait_time_;
}

double DataService::getTotalCPUusage(int idCPU)
{
	return total_processor_usage_[idCPU];
}

void DataService::addProcessCreated()
{
	processes_created_++;
}

void DataService::SetProcessesCreated(int value)
{
	processes_created_ = value;
}

void DataService::addProcessesTerminated()
{
	processes_terminated_++;
}

double DataService::getProcessesCreated()
{
	return processes_created_;
}

double DataService::getProcessesTerminated()
{
	return processes_terminated_;
}

void DataService::addDeviceDemand()
{
	if (processes_terminated_ > entry_phase_ || !is_entry_phase_)
	{
		++n_device_demand_;
		is_entry_phase_ = false;
	}
}

int DataService::getDeviceDemandCount()
{
	return n_device_demand_;
}

void DataService::SetLambda(double lambda)
{
	lambda_ = lambda;
}

void DataService::SetEntryPhase(int entry_phase)
{
	entry_phase_ = entry_phase;
}

void DataService::Display()
{
	using namespace std;

	cout << "Lambda: " << lambda_ << "\n";
	cout << "Faza poczatkowa: " << entry_phase_ << "\n";
	cout << "Czas symulacji: " << simulation_time_ << "\n";
	cout << "Liczba utworzonych procesow: " << processes_created_ << "\n";
	cout << "Liczba zakonczonych procesow: " << processes_terminated_ << "\n";
	cout << "Calkowite wykorzystanie procesora: \n"
		<< "\t1. " << total_processor_usage_[0] << "\n"
		<< "\t2. " << total_processor_usage_[1] << "\n";
	cout << "Liczba zadan dostepu do IO: " << n_device_demand_ << "\n";
	cout << "Sredni czas przetwarzania: " << avarge_processing_time_ << "\n";
	cout << "Sredni czas oczekiwania na urzadzenie: " << avarge_device_wait_time_ << "\n";
	cout << "Pojemnosc: " << total_capacity_ << "\n";
}

void DataService::SaveToFile(char* file_name)
{
	std::ofstream result_file(file_name);

	result_file << "Lambda: " << lambda_ << "\n";
	result_file << "Faza poczatkowa: " << entry_phase_ << "\n";
	result_file << "Czas symulacji: " << simulation_time_ << "\n";
	result_file << "Liczba utworzonych procesow: " << processes_created_ << "\n";
	result_file << "Liczba zakonczonych procesow: " << processes_terminated_ << "\n";
	result_file << "Calkowite wykorzystanie procesora: \n"
		<< "\t1. " << total_processor_usage_[0] << "\n"
		<< "\tq. " << total_processor_usage_[1] << "\n";
	result_file << "Liczba zadan dostepu do IO: " << n_device_demand_ << "\n";
	result_file << "Sredni czas przetwarzania: " << avarge_processing_time_ << "\n";
	result_file << "Sredni czas oczekiwania na urzadzenie: " << avarge_device_wait_time_ << "\n";
	result_file << "Pojemnosc: " << total_capacity_ << "\n";

	result_file.close();

	std::cout << "\nSaved data to " << file_name << ".\n";
}

void DataService::SampleProcessInSystem(double clock_time)
{
	sample_clock_time.push_back(clock_time);
	sample_procces_in_system.push_back(processes_created_ - processes_terminated_);
}

void DataService::SaveSampleProcessInSystemToFile()
{
	char * file_name = "stats/entry_phase_data.txt";
	std::ofstream result_file(file_name);

	result_file << "Clock_time Process_in_system\n";

	for (int i = 0; i < sample_procces_in_system.size(); ++i)
	{
		result_file<< sample_clock_time[i] << " " << sample_procces_in_system[i] << "\n";
	}


	result_file.close();
}

void DataService::SaveProcessorWaitToVectors(std::vector<double>& lambda_vector, std::vector<double>& avarge_processor_wait_vector, std::vector<double>& max_processor_wait_vector)
{
	lambda_vector.push_back(lambda_);
	avarge_processor_wait_vector.push_back(avargee_processor_wait_time_);
	max_processor_wait_vector.push_back(max_processor_wait_time_);
}

void DataService::SaveVectorsToFile(std::vector<double>& lambda_vector, std::vector<double>& avarge_processor_wait_vector, std::vector<double>& max_processor_wait_vector)
{
	char * file_name = "stats/processor_wait_times_with_lambda.txt";
	std::ofstream result_file(file_name);

	result_file << "Lambda Avarge_processor_wait_time Max_processor_wait_time\n";

	for (int i = 0; i < lambda_vector.size(); ++i)
	{
		result_file << lambda_vector[i] << " " << avarge_processor_wait_vector[i] << " " << max_processor_wait_vector[i] << "\n";
	}

	result_file.close();
}
