#pragma once
#include <vector>

class DataService
{
public:
	DataService();
	~DataService();

	void SetSimulationTime(double time);
	void addTotalProcessingTime(double time);
	void ComputeAvargeProcessingTime();
	void ComputeTotalCapacity();
	void ComputeAvargeProcessorWaitTime();
	void ComputeAvargeDeviceWaitTime();
	void addTotalProcessorWaitTime(double time);
	void addTotalDeviceWaitTime(double time);
	void addTotalProcessorUsage(double value, int idCPU);
	double getSimulationTime();
	double getTotalProcessingTime();
	double getTotalCapacity();
	double getTotalCPUtime();
	double getTotalDeviceTime();
	double getTotalCPUusage(int idCPU);
	void addProcessCreated();
	void SetProcessesCreated(int value);
	void addProcessesTerminated();
	double getProcessesCreated();
	double getProcessesTerminated();
	void addDeviceDemand();
	int getDeviceDemandCount();
	void SetLambda(double lambda);
	void SetEntryPhase(int entry_phase);

	void Display();
	void SaveToFile(char* file_name = "stats/results.txt");

	// for compute starting phase
	void SampleProcessInSystem(double clock_time);
	void SaveSampleProcessInSystemToFile();
	void SaveProcessorWaitToVectors(std::vector<double> &lambda_vector, std::vector<double> &avarge_processor_wait_vector, std::vector<double> &max_processor_wait_vector);

	static void SaveVectorsToFile(std::vector<double> &lambda_vector, std::vector<double> &avarge_processor_wait_vector, std::vector<double> &max_processor_wait_vector);

private:

	// ca�kowity czas symulacji
	double simulation_time_;
	// czas przetwarzania systemue
	double total_processing_time_;
	// sredni czas przetwarzania systemue
	double avarge_processing_time_;
	// calkowita przepustowosc systemu (dla wszystkich zakonczonych procesow)
	double total_capacity_;
	// calkowity czas oczekiwania na CPU (suma dla wszystkich procesow)
	double total_processor_wait_time_;
	// calkowity czas oczekiwania na CPU (suma dla wszystkich procesow)
	double avargee_processor_wait_time_;
	// maxymalny czas oczekiwania na CPU
	double max_processor_wait_time_;
	// wykorzystanie procesorow (kazdy osobno)
	double total_processor_usage_[2];
	// calkowity czas czekania na urzadzenie (suma dla wszystkich procesow)
	double total_device_wait_time_;
	// sredni czas czekania na urzadzenie (suma dla wszystkich procesow)
	double avarge_device_wait_time_;
	// ilosc wszystkich stworzonych procesow
	int processes_created_;
	// ilosc wszystkich ZAKONCZONYCH procesow
	int processes_terminated_;
	// ilosc zadan dostepu do IO
	int n_device_demand_;
	// intensywnosc lambda_
	double lambda_;
	// faza poczatkowa
	int entry_phase_;
	bool is_entry_phase_;

	std::vector<double> sample_procces_in_system;
	std::vector<double> sample_clock_time;
};
