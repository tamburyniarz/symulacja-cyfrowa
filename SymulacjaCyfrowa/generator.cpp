#include "generator.h"
#include <cstdlib>
#include <ostream>
#include <iostream>
#include <vector>
#include <fstream>

Generator::Generator(unsigned long seed)
{
	seed_ = seed;
	prev_number_ = seed_;
}


Generator::~Generator()
{
}

unsigned long Generator::getInteger()
{
	prev_number_ = (A_ * prev_number_) % M_;
	return prev_number_;
}

unsigned long Generator::getInteger(int range)
{
	return getInteger(0, range);
}

unsigned long Generator::getInteger(int min, int max)
{
	return (getInteger() % (max - min)) + min;
}

double Generator::getUniform()
{
	return double(getInteger()) / double(M_);
}

double Generator::getExpo()
{
	return getExponential(lambda_);
}

double Generator::getExponential(const double& lambda)
{
	return -log(getUniform()) / lambda_;
}

void Generator::TestUniform(int samples, int range)
{
	std::vector<double> values;
	for (int i = 0; i < range; ++i)
	{
		values.push_back(0);
	}
	for (int i = 0; i < 5000; ++i)
	{
		long temp = long(getUniform() * range);
		++values[temp];
	}
	double max = 0;
	for (int i = 0; i < range; ++i)
	{
		if (values[i] > max)
			max = values[i];
	}

	for (int i = 0; i < range; ++i)
	{
		std::cout << " " << i << ": ";
		double postep = double(values[i]) / double(max) * 100.0;
		for (int j = 0; j < postep; ++j)
		{
			std::cout << "|";
		}
		std::cout << std::endl;
	}
}

void Generator::TestExponential(const double& lambda, int samles, int range)
{
	std::vector<double> values;
	for (int i = 0; i < range; ++i)
	{
		values.push_back(0);
	}
	for (int i = 0; i < samles; ++i)
	{
		long temp = long(getExponential(lambda));
		if (temp > range)
		{
			continue;
		}
		++values[temp];
	}
	double max = 0;
	for (int i = 0; i < range; ++i)
	{
		if (values[i] > max)
			max = values[i];
	}

	for (int i = 0; i < range; ++i)
	{
		std::cout << " " << i << ": ";
		double postep;
		if (max != 0)
			postep = double(values[i]) / double(max) * 100.0;
		else
			postep = 0.0;
		for (int j = 0; j < postep; ++j)
		{
			std::cout << "|";
		}
		std::cout << std::endl;
	}
}

void Generator::setLambda(double lambda)
{
	lambda_ = lambda;
}

void Generator::createSeedFile(int n_seeds, char* file_name)
{
	std::ofstream seed_file(file_name);

	int interval = 10;

	if (seed_file.is_open())
	{
		for (int i = 0; i < n_seeds; ++i)
		{
			for (int j = 0; j < interval; ++j)
			{
				seed_file << getInteger() << " ";
			}
			if (i % (n_seeds / 10) == 1)
				std::cout << ".";
		}
		seed_file.close();
	}

	std::cout << "\nGenerated " << n_seeds << " seeds and saved to " << file_name << ".\n";
}
