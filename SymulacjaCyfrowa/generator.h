#pragma once
class Generator
{
public:
	Generator(unsigned long seed);
	~Generator();

	unsigned long getInteger();
	unsigned long getInteger(int range);
	unsigned long getInteger(int min, int max);
	double getUniform();
	double getExpo();
	inline double getExponential(const double & lambda);
	void TestUniform(int samples, int range);
	void TestExponential(const double & lambda, int samles, int range);

	void setLambda(double lambda);

	void createSeedFile(int n_seeds, char * file_name = "stats/seeds.txt");

private:
	const int A_ = 16807;
	const int M_ = 2147483647;

	double lambda_;
	unsigned long seed_;
	unsigned long prev_number_;
};

