#include "io_device.h"



IODevice::IODevice(std::string name)
{
	name_ = name;
	active_rpocess_ = nullptr;
}


IODevice::~IODevice()
{
}

void IODevice::AssignProcess(Process* process)
{
	if (active_rpocess_ == nullptr)
		active_rpocess_ = process;
}

void IODevice::DismissProcess()
{
	if (active_rpocess_ != nullptr)
		active_rpocess_ = nullptr;
}

bool IODevice::isBusy()
{
	if (active_rpocess_ == nullptr)
		return false;
	return true;
}

Process * IODevice::getActiveProcess()
{
	return active_rpocess_;
}
