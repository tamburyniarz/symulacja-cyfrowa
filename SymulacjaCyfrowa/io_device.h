#pragma once
#include "process.h"
#include <string>

class IODevice
{
public:
	IODevice(std::string name);
	~IODevice();
	void AssignProcess(Process * process);
	void DismissProcess();
	bool isBusy();
	Process * getActiveProcess();
private:
	std::string name_;
	Process * active_rpocess_;
};

