#include <iostream>
#include "simulator.h"
#include "InOutProcessQueue.h"
#include "data_service.h"




int main()
{
	int n_simulations = 20;
	int n_process = 20 * 1000;
	int entry_phase = 30;

	//	Generator* generator = new Generator(14);
	//	generator->TestUniform(1000, 50);
	//	generator->TestExponential(1, 10000, 50);
	//	generator->createSeedFile(n_simulations * (1 + 1 + n_process));

	//	DataService getDataService;

	//	getDataService.SaveToFile();

	std::vector<double> lambda_vector_;
	std::vector<double> avarge_processor_wait_vector_;
	std::vector<double> max_processor_wait_vector_;

	clock_t begin = clock();

	static int simulation_number = 1;
	double lambda = 0.06;

	Simulator* simulator = new Simulator(lambda, entry_phase);

	simulator->RunSimulation();

	DataService data_service = *simulator->getDataService();

	data_service.SaveProcessorWaitToVectors(lambda_vector_, avarge_processor_wait_vector_, max_processor_wait_vector_);
	DataService::SaveVectorsToFile(lambda_vector_, avarge_processor_wait_vector_, max_processor_wait_vector_);


	std::cout << "Proces created: " << data_service.getProcessesCreated() << "\n";



	delete simulator;

	clock_t end = clock();
	double whole_time = double(end - begin);
	std::cout << "whole_time: " << whole_time << " ms.\n";


	std::cout << "Finito! \n";

	getchar();
	return 0;
}
