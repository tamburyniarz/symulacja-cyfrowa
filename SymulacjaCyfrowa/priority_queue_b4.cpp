#include "priority_queue_b4.h"
#include <string>
#include <iostream>


bool ComparePriorityToDevice::operator()(Process* left, Process* right) const
{
	if (left->priority_to_device < right->priority_to_device) return true;
	if (left->priority_to_device > right->priority_to_device) return false;

	if (left->priority_to_device_random < right->priority_to_device_random) return true;
	if (left->priority_to_device_random > right->priority_to_device_random) return false;

	return false;
}

PriorityQueueB4::PriorityQueueB4(Generator* generator)
{
	generator_ = generator;
}


PriorityQueueB4::~PriorityQueueB4()
{
}

Process* PriorityQueueB4::top()
{
	if (!queue_.empty())
		return queue_.top();
	else return nullptr;
}

void PriorityQueueB4::push(Process* process)
{
	if (process != nullptr)
	{
		process->priority_to_device_random = generator_->getInteger(1000);
		queue_.push(process);
	}
}

void PriorityQueueB4::pop()
{
	if (!queue_.empty())
		queue_.pop();
}

bool PriorityQueueB4::empty()
{
	return queue_.empty();
}

int PriorityQueueB4::size()
{
	return queue_.size();
}

void PriorityQueueB4::update()
{
	std::priority_queue<Process *, std::vector<Process *>, ComparePriorityToDevice> queue_copy = queue_;

	while (!queue_.empty())
	{
		Process * process_ptr = queue_.top();
		process_ptr->priority_to_device = -process_ptr->TPO + process_ptr->TO;
		queue_.pop();
	}
	while (!queue_copy.empty())
	{
		Process * process_ptr = queue_copy.top();
		queue_.push(process_ptr);
		queue_copy.pop();
	}
}

void PriorityQueueB4::ShowAll()
{
	using namespace std;

	cout << "Procesy w kolejce do urzadzenia IO:" << endl;

	std::priority_queue<Process *, std::vector<Process *>, ComparePriorityToDevice> queue_copy = queue_;

	cout << "Queue size: " << queue_copy.size() << "\n";

	while (!queue_copy.empty())
	{
		cout << "Proces " << queue_copy.top()->id << "\n"
			<< " -> Priorytet: " << queue_copy.top()->priority_to_device << "\n"
			<< " -> Priority random: " << queue_copy.top()->priority_to_device_random << "\n";
		queue_copy.pop();
	}
	cout << endl;

	//
	//
	//	std::vector<std::string> string_buffer;
	//	if (queue_.empty()) {
	//		string_buffer.push_back("\tLista jest pusta.\n");
	//	}
	//	std::priority_queue < Process *, std::vector < Process * >, ComparePriorityToDevice > temp_priority_queue = queue_;
	//	while (!temp_priority_queue.empty()) {
	//		string_buffer.push_back(string("Element id ") + to_string(temp_priority_queue.top()->id) + string("\n"));
	//		string_buffer.push_back(string("\tpriority: ") + to_string(temp_priority_queue.top()->priority_to_device) + string("\n"));
	//		string_buffer.push_back(string("\tTime in device queue: ") + to_string(temp_priority_queue.top()->time_start_device_queue) + string("\n"));
	//		string_buffer.push_back(string("\tDevice id wanted: ") + to_string(temp_priority_queue.top()->device_id) + string("\n"));
	//		string_buffer.push_back(string("\tDevice id wanted time: ") + to_string(temp_priority_queue.top()->TPO) + string("\n"));
	//		temp_priority_queue.pop();
	//	}
	//
	//	for (int i=0; string_buffer.size(); ++i)
	//	{
	//		cout << string_buffer[i];
	//	}
	//	// TODO
	//	Computer::SaveToFile(string_buffer);
}
