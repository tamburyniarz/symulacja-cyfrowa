#pragma once
#include "process.h"
#include "generator.h"
#include <queue>


struct ComparePriorityToDevice
{
	bool operator()(Process* left, Process* right) const;
};

class PriorityQueueB4
{
public:
	PriorityQueueB4(Generator * generator);
	~PriorityQueueB4();
	Process * top();
	void push(Process * process);
	void pop();
	bool empty();
	int size();
	void update();
	void ShowAll();
private:
	Generator * generator_;

	std::priority_queue< Process *, std::vector< Process * >, ComparePriorityToDevice > queue_;

};

