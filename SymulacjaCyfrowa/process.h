#pragma once
struct  Process
{
	Process(int id, double TPW)
	{
		this->id = id;
		this->TPW = TPW;

		device_id = 99;

		time_start_processor_queue = 0.0;
		time_start_device_queue = 0.0;
		time_in_processor_queue = 0.0;
		time_in_device_queue = 0.0;
	}
	int id;

	// times
	int TPW; // how much time process need processor
	int TPIO; // after this time process wants I/O device
	int TPO; // how much time process need I/O device
	int TPK; // already in processor time
	int TO; // enter time to io_device_queue
	int device_id; // id of wanted device

	int priority_to_processor_random; // random
	int priority_to_device; // -TPO + TO
	int priority_to_device_random; // it will be used when priority_to_device equeal

	double born_time; // when procces enter to system
	double lifetime; // how long process was in simulation

	double time_start_processor_queue; // time when process enter to queue to processor
	double time_start_device_queue; // time when process enter to queue to device
	double time_in_processor_queue; // time when process enter to queue to processor
	double time_in_device_queue; // time when process enter to queue to device
};

