#include "processor.h"
#include <iostream>


Processor::Processor()
{
	TPKK_ = 0;
	active_process_ = nullptr;
}


Processor::~Processor()
{
}

void Processor::AssignProcess(Process * process)
{
	if (active_process_ == nullptr)
	{
		active_process_ = process;
		TPKK_ = process->TPW;
		process->TPK = TPKK_; // TODO its smell
	}
}

void Processor::DismissProcess()
{
	if (active_process_ != nullptr)
	{
		active_process_ = nullptr;
		TPKK_ = 0;
	}
}

bool Processor::isBusy() const
{
	if (active_process_ == nullptr)
		return false;
	return true;
}

/*
*
* Getters and setters
*
*/

Process * Processor::getActiveProcess() const
{
	return active_process_;
}

int Processor::getTPK() const
{
	return TPKK_;
}

double Processor::getProcesssEntryTime() const
{
	return process_entry_time_;
}

void Processor::setProcessEntryTime(double process_entry_time)
{
	process_entry_time_ = process_entry_time;
}

double Processor::getProcessTotalTime() const
{
	return process_total_time_;
}

void Processor::setProcessTotalTime(double process_total_time)
{
	process_total_time_ = process_total_time;
}
