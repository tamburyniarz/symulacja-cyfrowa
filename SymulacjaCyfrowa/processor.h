#pragma once
#include "process.h"

class Processor
{
public:
	Processor();
	~Processor();
	void AssignProcess(Process * process);
	void DismissProcess();
	bool isBusy() const;

	// Getters and setters
	int getTPK() const;
	Process * getActiveProcess() const;
	double getProcesssEntryTime() const;
	void setProcessEntryTime(double process_entry_time);
	double getProcessTotalTime() const;
	void setProcessTotalTime(double process_total_time);
private:
	int TPKK_; // how long process can use processor; in this case TPK = TPW_
				// it means that process can use processor as long as needed
	Process * active_process_;

	double process_entry_time_; // TODO may be not necessery
	double process_total_time_;
};
