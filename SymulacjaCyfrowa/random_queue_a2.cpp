#include "random_queue_a2.h"
#include <string>
#include <iostream>


bool ComparePriorityToProcessor::operator()(Process* left, Process* right) const
{
	//kolejno�� - malej�co
	if (left->priority_to_processor_random < right->priority_to_processor_random) return true;

	if (left->priority_to_processor_random > right->priority_to_processor_random) return false;

	return false;
}

RandomQueueA2::RandomQueueA2(Generator* generator)
{
	generator_ = generator;
}


RandomQueueA2::~RandomQueueA2()
{
}

Process* RandomQueueA2::top()
{
	if (!queue_.empty())
		return queue_.top();
	else return nullptr;
}

void RandomQueueA2::push(Process* process)
{
	// TODO change generator
	if (process != nullptr)
	{
		process->priority_to_processor_random = generator_->getInteger(1000);
		queue_.push(process);
	}
}

void RandomQueueA2::pop()
{
	if (!queue_.empty())
		queue_.pop();
}

bool RandomQueueA2::empty()
{
	return queue_.empty();
}

int RandomQueueA2::size()
{
	return queue_.size();
}

void RandomQueueA2::ShowAll()
{
	using namespace std;

	cout << "Procesy w kolejce do procesora:" << endl;

	std::priority_queue<Process *, std::vector<Process *>, ComparePriorityToProcessor> queue_copy = queue_;

	cout << "Queue size: " << queue_copy.size() << "\n";

	while (!queue_copy.empty())
	{
		cout << "Proces " << queue_copy.top()->id << "\n"
			<< " -> Priority random: " << queue_copy.top()->priority_to_processor_random << "\n";
		queue_copy.pop();
	}
	cout << endl;
}
