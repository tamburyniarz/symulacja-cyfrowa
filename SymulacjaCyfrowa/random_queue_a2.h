#pragma once
#include <queue>
#include "process.h"
#include "generator.h"

struct ComparePriorityToProcessor
{
	bool operator()(Process* left, Process* right) const;

};

class RandomQueueA2
{
public:
	RandomQueueA2(Generator * generator);
	~RandomQueueA2();
	Process * top();
	void push(Process * process);
	void pop();
	bool empty();
	int size();
	void ShowAll(); // debug
private:
	Generator * generator_;

	std::priority_queue< Process *, std::vector < Process * >, ComparePriorityToProcessor > queue_;
};

