#include "simulator.h"
#include <iostream>

Simulator::Simulator(double lambda, int entry_phase)
{
	// TODO create seed_ table
	lambda_ = lambda;
	entry_phase_ = entry_phase;

	next_event_time_ = 0.0;

	InitEvents();
}

Simulator::~Simulator()
{
	DeleteEvents();
}

void Simulator::RunSimulation()
{
	bool event_flag = false;
	bool is_step = false;
	bool end_simulation = false;
	int process_id = 0;

	int steps_counter = 0;

	clock_t begin = clock();

	while (!end_simulation)
	{
		event_flag = false;

		if (is_step)
		{
			std::cout << "\n### STEP NO. " << ++steps_counter << " ### \n";
			std::cout << "\n\t>> CLOCK TIME: " << computer_->getClockTime() << ". \n";
		}

		// Check time events
		if (time_event_new_process_->Execute(process_id, is_step))
		{
			process_id++;
			event_flag = true;
		}
		// TODO Loops pleasee!
		if (time_event_processor_release_->Execute(0, is_step)) event_flag = true;
		if (time_event_processor_release_->Execute(1, is_step)) event_flag = true;
		// TODO More looooops.... i will cry...
		if (time_event_device_release_->Execute(0, is_step)) event_flag = true;
		if (time_event_device_release_->Execute(1, is_step)) event_flag = true;
		if (time_event_device_release_->Execute(2, is_step)) event_flag = true;
		if (time_event_device_release_->Execute(3, is_step)) event_flag = true;
		if (time_event_device_release_->Execute(4, is_step)) event_flag = true;
		// TODO one more little loopy please
		if (time_event_process_termination_->Execute(0, is_step)) event_flag = true;
		if (time_event_process_termination_->Execute(1, is_step)) event_flag = true;

		// Check conditional events
		if (conditional_event_add_process_to_processor_->Execute(0, is_step)) event_flag = true;
		if (conditional_event_add_process_to_processor_->Execute(1, is_step)) event_flag = true;

		if (conditional_event_add_process_to_device_->Execute(0, is_step)) event_flag = true;
		if (conditional_event_add_process_to_device_->Execute(1, is_step)) event_flag = true;
		if (conditional_event_add_process_to_device_->Execute(2, is_step)) event_flag = true;
		if (conditional_event_add_process_to_device_->Execute(3, is_step)) event_flag = true;
		if (conditional_event_add_process_to_device_->Execute(4, is_step)) event_flag = true;

		// Check next time event
		next_event_time_ = time_event_new_process_->getEventTime();

		if (time_event_processor_release_->getEventTime(0) < next_event_time_ && time_event_processor_release_->getEventTime(0) != -1.0) next_event_time_ = time_event_processor_release_->getEventTime(0);
		if (time_event_processor_release_->getEventTime(1) < next_event_time_ && time_event_processor_release_->getEventTime(1) != -1.0) next_event_time_ = time_event_processor_release_->getEventTime(1);

		if (time_event_device_release_->getEventTime(0) < next_event_time_ && time_event_device_release_->getEventTime(0) != -1.0) next_event_time_ = time_event_device_release_->getEventTime(0);
		if (time_event_device_release_->getEventTime(1) < next_event_time_ && time_event_device_release_->getEventTime(1) != -1.0) next_event_time_ = time_event_device_release_->getEventTime(1);
		if (time_event_device_release_->getEventTime(2) < next_event_time_ && time_event_device_release_->getEventTime(2) != -1.0) next_event_time_ = time_event_device_release_->getEventTime(2);
		if (time_event_device_release_->getEventTime(3) < next_event_time_ && time_event_device_release_->getEventTime(3) != -1.0) next_event_time_ = time_event_device_release_->getEventTime(3);
		if (time_event_device_release_->getEventTime(4) < next_event_time_ && time_event_device_release_->getEventTime(4) != -1.0) next_event_time_ = time_event_device_release_->getEventTime(4);

		if (time_event_process_termination_->getEventTime(0) < next_event_time_ && time_event_process_termination_->getEventTime(0) != -1.0) next_event_time_ = time_event_process_termination_->getEventTime(0);
		if (time_event_process_termination_->getEventTime(1) < next_event_time_ && time_event_process_termination_->getEventTime(1) != -1.0) next_event_time_ = time_event_process_termination_->getEventTime(1);

		if (is_step)
			std::cout << "\n\t>>Next time event time: " << next_event_time_ << "\n";

		if (!event_flag)
		{
			// Increse clock_time
			computer_->setClockTime(next_event_time_);
		}

		if (data_service_->getProcessesCreated() >= 20 * 1000)
		{
			end_simulation = true;
		}

		static int miles_stones_for_phae = 0;
		if (data_service_->getProcessesCreated() > miles_stones_for_phae)
		{
			data_service_->SampleProcessInSystem(computer_->getClockTime());
			miles_stones_for_phae += 20 * 1000 / 500;
		}

			steps_counter++;
	}
	clock_t end = clock();
	real_simulation_time_ = double(end - begin);
	std::cout << "Real simulation time: " << real_simulation_time_ << " ms.\n";

	data_service_->SetSimulationTime(computer_->getClockTime());
	data_service_->ComputeTotalCapacity();
	data_service_->ComputeAvargeProcessingTime();
	data_service_->ComputeAvargeProcessorWaitTime();
	data_service_->ComputeAvargeDeviceWaitTime();
	std::cout << "zadnia: " << data_service_->getDeviceDemandCount() << "\n";

	data_service_->SaveSampleProcessInSystemToFile();

	data_service_->Display();
	data_service_->SaveToFile();
}

void Simulator::RunDiffrentLambda()
{
	std::vector<double> lambda_vector_;
	std::vector<double> avarge_processor_wait_vector_;
	std::vector<double> max_processor_wait_vector_;

//	data_service.SaveProcessorWaitToVectors(lambda_vector_, avarge_processor_wait_vector_, max_processor_wait_vector_);
	DataService::SaveVectorsToFile(lambda_vector_, avarge_processor_wait_vector_, max_processor_wait_vector_);
}

DataService* Simulator::getDataService() const
{
	return data_service_;
}

void Simulator::InitEvents()
{
	//Generators //TODO
	int seed = 12;
	generator_ = new Generator(seed);
	generator_->setLambda(lambda_);

	// Stats
	data_service_ = new DataService();
	data_service_->SetLambda(lambda_);
	data_service_->SetEntryPhase(entry_phase_);

	// Computer
	computer_ = new Computer(generator_, data_service_);


	time_event_new_process_ = new TimeEventNewProcess(computer_, generator_, data_service_);
	time_event_processor_release_ = new TimeEventProcessorRelease(computer_, generator_, data_service_);
	time_event_device_release_ = new TimeEventDeviceRelease(computer_);
	time_event_process_termination_ = new TimeEventProcessTermination(computer_, data_service_);

	conditional_event_add_process_to_processor_ = new ConditionalEventAddProcessToProcessor(computer_, generator_, data_service_, time_event_processor_release_, time_event_process_termination_);
	conditional_event_add_process_to_device_ = new ConditionalEventAddProcessToDevice(computer_, generator_, data_service_, time_event_device_release_);
}

void Simulator::DeleteEvents()
{
	delete time_event_new_process_;
	delete time_event_processor_release_;
	delete time_event_device_release_;
	delete time_event_process_termination_;

	delete conditional_event_add_process_to_processor_;
	delete conditional_event_add_process_to_device_;
}
