#pragma once
#include "computer.h"

#include "time_event_new_process.h"
#include "time_event_processor_release.h"
#include "time_event_device_reealse.h"
#include "time_event_process_termination.h"

#include "conditional_event_add_process_to_processor.h"
#include "conditional_event_add_process_to_device.h"
#include "data_service.h"
#include <ctime>

class Simulator
{
public:
	Simulator(double lambda, int entry_phase);
	~Simulator();

	void RunSimulation();
	void RunDiffrentLambda();

	void UpdateSimulationTime(); // TODO make it private ?

	DataService* getDataService() const;
private:
	Computer * computer_;
	Generator * generator_;
	DataService * data_service_;

	double lambda_;
	int entry_phase_;

	double next_event_time_;
	double real_simulation_time_;

	TimeEventNewProcess * time_event_new_process_;
	TimeEventProcessorRelease * time_event_processor_release_;
	TimeEventDeviceRelease * time_event_device_release_;
	TimeEventProcessTermination * time_event_process_termination_;

	ConditionalEventAddProcessToProcessor * conditional_event_add_process_to_processor_;
	ConditionalEventAddProcessToDevice * conditional_event_add_process_to_device_;

	void InitEvents();
	void DeleteEvents();
};

