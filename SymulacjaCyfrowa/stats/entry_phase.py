import matplotlib.pyplot as plt

process_in_system = []
clock_time = []

in_file = "entry_phase_data.txt"

with open(in_file) as f:
    is_first_line = True
    for line in f: # read rest of lines
        if(is_first_line):
            plt.xlabel(line.split()[0])
            plt.ylabel(line.split()[1])
            is_first_line = False
        else:
            clock_time.append(float(line.split()[0]))
            process_in_system.append(float(line.split()[1]))

plt.plot(clock_time, process_in_system)
plt.savefig("entry_phase_plot.png")
plt.show()

print("Done")
