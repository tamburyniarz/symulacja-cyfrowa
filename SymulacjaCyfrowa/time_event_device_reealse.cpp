#include "time_event_device_reealse.h"
#include <iostream>

TimeEventDeviceRelease::TimeEventDeviceRelease(Computer * computer)
{
	computer_ = computer;

	for (int i = 0; i<Computer::N_IO_DEVICES; ++i)
	{
		event_time_[i] = -1.0;
	}
}

TimeEventDeviceRelease::~TimeEventDeviceRelease()
{
}

bool TimeEventDeviceRelease::Execute(int device_id, bool step)
{
	double clock_time = computer_->getClockTime();
	if (abs(event_time_[device_id] - clock_time) < 0.00001 /* it's safer then == 0 because accuracy*/)
	{
		if (step)
			std::cout << "\n*TimeEvent* Device Release: \n";
		
		IODevice * device_ptr = computer_->getDevices()[device_id];
		Process * process_ptr = device_ptr->getActiveProcess();

		device_ptr->DismissProcess();
		if (step)
			std::cout << "\t>> Device released. \n";

		computer_->getQueueToProcessor()->push(process_ptr);

		process_ptr->time_start_processor_queue = clock_time;
		if (step)
			std::cout << "\t>> Process returned to processor queue. \n";

		event_time_[device_id] = -1; //TODO MAX_VALUE ?

		return true;
	}
	return false;
}

/*
*
* Getters and setters
*
*/

double TimeEventDeviceRelease::getEventTime(int device_id)
{
	return event_time_[device_id];
}

void TimeEventDeviceRelease::setEventTime(int device_id, double time)
{
	event_time_[device_id] = time;
}
