#pragma once
#include "computer.h"
class TimeEventDeviceRelease
{
public:
	TimeEventDeviceRelease(Computer * computer);
	~TimeEventDeviceRelease();
	bool Execute(int device_id, bool step); //TODO step change

	// Getters and setters
	double getEventTime(int device_id);
	void setEventTime(int device_id, double time);
private:
	Computer * computer_;
	double event_time_[Computer::N_IO_DEVICES];
};

