#include "time_event_new_process.h"

#include <iostream>

TimeEventNewProcess::TimeEventNewProcess(Computer* computer, Generator* generator, DataService* data_service)
{
	computer_ = computer;
	generator_ = generator;
	data_service_ = data_service;

	event_time_ = -0.0;
	TPG_ = -1.0; // TODO MAX_VALUE ?
}

TimeEventNewProcess::~TimeEventNewProcess()
{
}

bool TimeEventNewProcess::Execute(int process_id, bool step)
{
	double clock_time = computer_->getClockTime();
	if(abs(event_time_ - clock_time) < 0.00001 /* it's safer then == 0 because accuracy*/)
	{
		if (step)
			std::cout << "\n*TimeEvent* New Process: \n";

		Process * new_process = new Process(process_id, generator_->getInteger(1,51));
		// Data
		data_service_->addProcessCreated();
		
		new_process->born_time = clock_time;
		if (step)
			std::cout << "\t>> New process arrival. \n";

		computer_->getQueueToProcessor()->push(new_process);
		new_process->time_start_processor_queue = clock_time;
		if (step)
			std::cout << "\t>> Added new process to processor queue. \n";

		// Calculate new TPG - new process generating time
		TPG_ = generator_->getExpo();  // TODO Lambda variable
		event_time_ = clock_time + TPG_;
		if (step)
			std::cout << "\t>> Next process arrival time: "<< event_time_ << "\n";

		return true;
	}
	return false;
}


/*
*
* Getters and seters
*
*/

double TimeEventNewProcess::getEventTime() const
{
	return event_time_;
}
