#pragma once
#include "computer.h"
#include "generator.h"
#include "data_service.h"

class TimeEventNewProcess
{
public:
	TimeEventNewProcess(Computer * computer, Generator * generator, DataService * data_service);
	~TimeEventNewProcess();
	bool Execute(int process_id, bool step /* TODO bool step global */);

	// Getters and setters
	double getEventTime() const;
private:
	Computer * computer_;
	Generator * generator_;
	DataService * data_service_; // TODO its not neccesary

	double event_time_;
	double TPG_;

};

