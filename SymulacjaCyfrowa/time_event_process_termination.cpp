#include "time_event_process_termination.h"
#include <iostream>

TimeEventProcessTermination::TimeEventProcessTermination(Computer* computer, DataService* data_service)
{
	computer_ = computer;
	data_service_ = data_service;

	termination_count_ = 0;

	for (int i = 0; i<Computer::N_PROCESSORS; ++i)
	{
		event_time_[i] = -1.0;
	}
}

TimeEventProcessTermination::~TimeEventProcessTermination()
{
}

bool TimeEventProcessTermination::Execute(int processor_id, bool step /* TODO it should be global*/)
{
	if (abs(event_time_[processor_id] - computer_->getClockTime()) < 0.00001 /* it's safer then == 0 because accuracy*/)
	{
		if (step)
			std::cout << "\n*TimeEvent* Process Termination: \n";

		Process * process_ptr = computer_->getProcessors()[processor_id]->getActiveProcess();
		Processor * processor_ptr = computer_->getProcessors()[processor_id];

		process_ptr->lifetime = computer_->getClockTime() - process_ptr->born_time;
		data_service_->addTotalProcessingTime(process_ptr->lifetime);

		processor_ptr->DismissProcess();
		if(step)
			std::cout << "\t>>Processor released. \n";

		processor_ptr->setProcessTotalTime(computer_->getClockTime() - processor_ptr->getProcesssEntryTime());

		delete process_ptr;
		if (step)
			std::cout << "\t>>Process deleted form system. \n";

		termination_count_++;
		
		data_service_->addProcessesTerminated();

		event_time_[processor_id] = -1; // TODO MAX_VALUE thing

		return true;
	}
	return false;
}

void TimeEventProcessTermination::setEventTime(int processor_id, double time)
{
	event_time_[processor_id] = time;
}

double TimeEventProcessTermination::getEventTime(int processor_id)
{
	return event_time_[processor_id];
}
