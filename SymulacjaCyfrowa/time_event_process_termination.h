#pragma once
#include "computer.h"
#include "data_service.h"
class TimeEventProcessTermination
{
/*
* Klasa reprezentujaca zdarzenie czasowe "usuniecie procesu z systemu"
*/

public:
	TimeEventProcessTermination(Computer * computer, DataService * data_service);
	~TimeEventProcessTermination();

	bool Execute(int processor_id, bool step);

	void setEventTime(int processor_id, double time);
	double getEventTime(int processor_id);

private:
	Computer * computer_;
	DataService * data_service_;

	int termination_count_;
	double event_time_[Computer::N_PROCESSORS];
};

