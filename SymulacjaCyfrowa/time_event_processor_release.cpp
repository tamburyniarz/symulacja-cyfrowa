#include "time_event_processor_release.h"
#include <iostream>

TimeEventProcessorRelease::TimeEventProcessorRelease(Computer* computer, Generator* generator, DataService* data_service)
{
	computer_ = computer;
	generator_ = generator;
	data_service_ = data_service;

	for (int i = 0; i<Computer::N_PROCESSORS; ++i)
	{
		event_time_[i] = -1.0;
		//		event_time_[i] = std::numeric_limits<double>::max();
	}
}

TimeEventProcessorRelease::~TimeEventProcessorRelease()
{
}

bool TimeEventProcessorRelease::Execute(int processor_id, bool step)
{
	double clock_time = computer_->getClockTime();

	if (abs(event_time_[processor_id] - clock_time) < 0.00001 /* it's safer then == 0 because accuracy*/)
	{
		if (step)
			std::cout << "\n*TimeEvent* Processor Release: \n";
		Processor * processor_ptr = computer_->getProcessors()[processor_id];
		Process * process_ptr = processor_ptr->getActiveProcess();

		processor_ptr->DismissProcess();
		if (step)
			std::cout << "\t>> Processor released. \n";

		processor_ptr->setProcessTotalTime(clock_time-processor_ptr->getProcesssEntryTime());
		
		data_service_->addTotalProcessorUsage(processor_ptr->getProcessTotalTime(), processor_id);

		// Add process to device_queue
		process_ptr->TO = clock_time;
		process_ptr->TPO = generator_->getInteger(process_ptr->TPK); // TODO change range ?

		if (process_ptr->TPO > 0)
			data_service_->addDeviceDemand();

		computer_->getQueuesToDevices()[process_ptr->device_id]->push(process_ptr);
		process_ptr->time_start_device_queue = clock_time;
		if (step)
			std::cout << "\t>> Added process to device queue. \n";

		event_time_[processor_id] = -1; // TODO MAX_VALUE ??

		return true;
	}
	return false;
}

/*
*
* Getters and setters 
*
*/

double TimeEventProcessorRelease::getEventTime(int processor_id) const
{
	return event_time_[processor_id];
}

void TimeEventProcessorRelease::setEventTime(int processor_id, double time)
{
	event_time_[processor_id] = time;
}
