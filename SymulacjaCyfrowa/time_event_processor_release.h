#pragma once
#include "computer.h"
#include "generator.h"
#include "data_service.h"
class TimeEventProcessorRelease
{
public:
	TimeEventProcessorRelease(Computer * computer, Generator * generator, DataService * data_service);
	~TimeEventProcessorRelease();

	bool Execute(int processor_id, bool step);

	// getters and setters
	double getEventTime(int processor_id) const;
	void setEventTime(int processor_id, double time);
private:
	Computer * computer_;
	Generator * generator_;
	DataService * data_service_;

	double event_time_[Computer::N_PROCESSORS];
};

